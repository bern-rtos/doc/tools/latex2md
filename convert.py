#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# pandoc does not do a very good job at converting my latex document, thus I
# wrote this script. The script is specific to the commands and structure I use 
# in Latex and cannot be used for any document.

import re
from bib_reader import BibReader
import os
import sys
import toml

# global variable, will be assigned in main
bib_parser = []

# functions for complex parse operations #######################################
# parse inner listing
def parse_listing(match):
	replacement = ''
	# there can be multiple minted listing in one listing environment
	match_all = re.findall(r'(\\listingpath{(.*?)})?\n?\\begin{minted}\[?(.*?)?\]?{(.*?)}\n([\s\S]+?)\n\\end{minted}', match.group(2))
	for m in match_all:
		# add listingpath if specified
		if m[1] != '':
			replacement += '*`{0}`*\n'.format(m[1])
		replacement += '```{0},noplaypen\n{1}\n```\n'.format(m[3], m[4])
	# but only one caption is allowed
	caption = re.search(r'\\caption\[?.*?\]?{(.*?)}', match.group(2))
	replacement += '*Listing: {0}*'.format(caption.group(1))
	return replacement

# parse cite keys to indices, i.e. [3]. Including page references,
# i.e. [3, p. 5], also create a link to the bibliography (bibliography.md)
def parse_cite(match):
	page_ref = match.group(2)
	cite_index = bib_parser.get_label_from_key(match.group(3))
	citation = '[[{0}'.format(cite_index)
	if page_ref != None:
		page = re.search(r'([0-9]+)', page_ref).group(1)
		if '~' in page_ref:
			citation +=', pp. {0}'.format(page)
		else:
			citation +=', p. {0}'.format(page)
	citation += ']](/bibliography.md#ref-{0})'.format(cite_index)
	return citation

def parse_citeauthor(match):
    key = match.group(1)
    return bib_parser.get_authors_from_key(key)

def parse_citetitle(match):
    key = match.group(1)
    return "*{0}*".format(bib_parser.get_title_from_key(key))

# simple regex for easier parse operations #####################################
re_fixes = []
# remove comments
re_fixes.append([r'%(.*)', ''])
# custom command replacements
re_fixes.append([r'\\iic', 'I²C'])
re_fixes.append([r'\\ucos', 'μC/OS'])
# replace chapter, sections, etc.
re_fixes.append([r'\\chapter\*?{(.*)}', r'# \1'])
re_fixes.append([r'\\section\*?{(.*)}', r'## \1'])
re_fixes.append([r'\\subsection\*?{(.*)}', r'### \1'])
re_fixes.append([r'\\subsubsection\*?{(.*)}', r'#### \1'])
# italics, bold and code
re_fixes.append([r'\\emph{(.*?)}', r'*\1*'])
re_fixes.append([r'\\textbf{(.*?)}', r'**\1**'])
re_fixes.append([r'\\texttt{(.*?)}', r'`\1`'])
re_fixes.append([r'\\mintinline{(.*?)}{(.*?)}', r'`\2`'])
# citations
re_fixes.append([r'\\cite(\[(.+?)\])?{(.+?)}', parse_cite])
re_fixes.append([r'\\citeauthor{(.+?)}', parse_citeauthor])
re_fixes.append([r'\\citetitle{(.+?)}', parse_citetitle])
# url
re_fixes.append([r'\\url{(http[s]?://)?(.*?)}', r'[\2](https://\2)'])
# autorefs
re_fixes.append([r'\\autoref{fig:(.*?)}', r'the figure'])
re_fixes.append([r'\\autoref{tab:(.*?)}', r'the table'])
re_fixes.append([r'\\autoref{lst:(.*?)}', r'the listing'])
# figure with caption
re_fixes.append([r'\\begin{figure}\[[\s\S]+?\][\s\S]+?(\\includegraphics\[?[\s\S]*?\]?{(.*?)}|\\subfile{images/(.*?)})[\s\S]+?\\caption\[?.*?\]?{(.*?)}[\s\S]*?\\end{figure}', r'![\2\3](/img/\2\3)<br/>*Figure: \4*'])
# use svg versions instead of pdf, eps
re_fixes.append([r'!\[(.*?).pdf\]\((.*?).pdf\)', r'![\1](\2.svg)'])
re_fixes.append([r'!\[(.*?).eps\]\((.*?).eps\)', r'![\1](\2.svg)'])
re_fixes.append([r'!\[(.*?).tex\]\((.*?).tex\)', r'![\1](\2.svg)'])
# listings/code sections
re_fixes.append([r'\\begin{[\w]+?listing}(\[[\s\S]+?\])?([\s\S]+?)\\end{[\w]+?listing}', parse_listing])
# we don't need `\_` in markdown
re_fixes.append([r'\\_', '_'])
# some other fixes
re_fixes.append([r'\|##\|', '##'])
# remove remaining latex commands
re_fixes.append([r'\\newpage', ''])
re_fixes.append([r'\\label{[\S]+?}', ''])
re_fixes.append([r'\\addcontentsline{[\S]+?}{[\S]+?}{[\S]+?}', ''])
re_fixes.append([r'\\vspace{(.+?)}', ''])
# the \circled{} command needs to handled differently for code and normal sections
# for code section we need to apply a fix script after html is generated
re_fixes.append([r'[|\'/]\\circled{([0-9]+?)}[|\'/]', r'circled{\1}']) 
re_fixes.append([r'\\circled{([0-9]+?)}', r'<text class="circled">\1</text>']) # text
# my special table that I converted to an svg
re_fixes.append([r'\\input{tables/(.+?).tex}', r'![\1](/img/\1.svg)'])


# parse within group
re_parses = []
# enumerate, itemize and description 
re_parses.append([r'(\\begin{enumerate}\[?.*?\]?(\n[\s\S]*?)\\end{enumerate})', r'\\item', r'1.'])
re_parses.append([r'(\\begin{itemize}\[?.*?\]?(\n[\s\S]*?)\\end{itemize})', r'\\item', r'*'])
re_parses.append([r'(\\begin{description}\[?.*?\]?(\n[\s\S]*?)\\end{description})', r'\\item\[(.*?)\]', r'**\1**'])


# parses latex files to markdown files, by applying regex from above
def convert(latex_file, markdown_file):
	with open(latex_file, 'r') as f:
		content = f.read()
		for re_fix in re_fixes:
			content = re.sub(re_fix[0], re_fix[1], content)
		for re_parse in re_parses:
			match_all = re.findall(re_parse[0], content)
			for match in match_all:
				original = match[0]
				new = re.sub(re_parse[1], re_parse[2], match[1]).replace('    ', '')
				content = content.replace(original, new)
			   

	with open(markdown_file, 'w') as f:
		f.seek(0)
		f.truncate()
		f.write(content)


################################################################################
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('You must provide a config.toml file!')
        sys.exit(-1)
    config_file = sys.argv[1]
    config = toml.load(config_file)

    source_dir = config.get('latex').get('chapter').get('directory')
    sources = config.get('latex').get('chapter').get('files')
    target_dir = config.get('markdown').get('directory')
    bib_dir = config.get('latex').get('bibliography').get('directory')
    bib_file = config.get('latex').get('bibliography').get('file')
    
    bib_parser = BibReader(os.path.join(bib_dir, bib_file + '.bib'))
    markdown_bib = os.path.join(target_dir, bib_file + '.md')
    
    for source in sources:
	    if source != '---':
		    convert(os.path.join(source_dir, source + ".tex"),
	                os.path.join(target_dir, source + ".md"))
        
    bib_parser.parse_to_markdown(markdown_bib)
		
		
