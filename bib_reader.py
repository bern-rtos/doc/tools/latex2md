#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pybtex.database.input import bibtex
import pybtex.plugin
import six
import io
import re


################################################################################
class BibReader:
    def __init__(self, bibliography_path):
        self._bibliography = bibliography_path
        self._parser = bibtex.Parser()
        self._bibdata = self._parser.parse_file(bibliography_path)
        self._pybtex_style = pybtex.plugin.find_plugin('pybtex.style.formatting', 'plain')()
        self._pybtex_backend = pybtex.plugin.find_plugin('pybtex.backends', 'markdown')()
        
        data = self._parser.parse_stream(six.StringIO(self._bibliography))
        data_formatted = self._pybtex_style.format_entries(data.entries.values())
        self._bib_list = list(data_formatted)
		
    def parse_to_markdown(self, markdown_path):
        data = self._parser.parse_stream(six.StringIO(self._bibliography))

        # write markdown bib
        data_formatted = self._pybtex_style.format_entries(data.entries.values())
        output = io.StringIO()
        self._pybtex_backend.write_to_stream(data_formatted, output)

        # format bibliography as table
        markdown = '# Bibliography\n|   |   |\n|---|---|\n'
        # one line per entry
        bib_text = re.sub(r'\n(?!\[[0-9])', r' ', output.getvalue())
        bib_text = re.sub(r'[\s]*(?=[\n])', r'', bib_text)
        # bib as table
        entries = re.sub(r'\[([0-9]+?)\](.*)', r'| <div id="ref-\1">[\1]</div> | \2 |', bib_text)
        markdown += entries
        with open(markdown_path, 'w') as f:
            f.seek(0)
            f.truncate()
            f.write(markdown)
			
    def get_label_from_key(self, key):
        entry = [e for e in self._bib_list if e.key == key]
        return entry[0].label
    
    def get_authors_from_key(self, key):
        authors = self._bibdata.entries[key].persons['author']
        names = [n.last_names[-1] for n in authors]
        if len(names) == 1:
            return names[-1].replace('{','').replace('}','')
        all_but_last = ', '.join(names[:-1])
        authors_text = ' and '.join([all_but_last, names[-1]])
        return authors_text.replace('{','').replace('}','')

    def get_title_from_key(self, key):
         title = self._bibdata.entries['rust-embedded:book'].fields['title']
         return title.replace('{','').replace('}','')
        
################################################################################
if __name__ == "__main__":
    parser = BibReader("/home/stefan/Documents/BfhDocs/doc-pa1/database/bibliography.bib")
    #parser.parse_to_markdown("/home/stefan/Documents/BfhDocs/doc-pa1/mdbook/src/bibliography.md")
    #print(parser.find_number_from_key('rust:lang'))
    #print(list(parser._bibdata.entries))
    print(parser.get_authors_from_key('rust-embedded:book'))


	
	
	
	
	
